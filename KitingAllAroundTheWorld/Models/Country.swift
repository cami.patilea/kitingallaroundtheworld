//
//  Country.swift
//  KitingAllAroundTheWorld
//
//  Created by Student on 19/04/2019.
//  Copyright © 2019 none. All rights reserved.
//

import UIKit

class Country : Codable {
    let name: String?
    
    init(name: String?) {
        self.name = name
    }
    
    init() {
        self.name = nil
    }
    
}
