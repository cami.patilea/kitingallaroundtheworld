//
//  ErrorResponse.swift
//  KitingAllAroundTheWorld
//
//  Created by Student on 16/04/2019.
//  Copyright © 2019 none. All rights reserved.
//

import UIKit

struct ErrorResponse : Decodable {
    let message: String
    let code: String
    let sentBodyParameters: String
}
