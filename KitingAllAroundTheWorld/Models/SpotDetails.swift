//
//  SpotDetails.swift
//  KitingAllAroundTheWorld
//
//  Created by Student on 19/04/2019.
//  Copyright © 2019 none. All rights reserved.
//

import UIKit


class SpotDetail{
    var id : String?
    var name: String?
    var longitude: Float?
    var latitude: Float?
    var windProbability: Int?
    var country : String?
    var whenToGo : String?
    var isFavorite: Bool?
   
    
    init(id: String?, name: String?, longitude:Float?, latitude: Float?, windProbability: Int?,  country: String?, whenToGo: String?, isFavorite: Bool?){
        self.id = id
        self.name = name
        self.longitude = longitude
        self.latitude = latitude
        self.windProbability = windProbability
        self.country = country
        self.whenToGo = whenToGo
        self.isFavorite = isFavorite
        
    }
    init(){
        self.id = nil
        self.name = nil
        self.longitude = nil
        self.latitude = nil
        self.windProbability = nil
        self.country = nil
        self.whenToGo = nil
        self.isFavorite = nil
        
    }
}
