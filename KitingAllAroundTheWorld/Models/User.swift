//
//  User.swift
//  KitingAllAroundTheWorld
//
//  Created by Student on 16/04/2019.
//  Copyright © 2019 none. All rights reserved.
//

import UIKit

class User : Codable {
    let token: String?
    let email: String?
    
    init(token: String?, email: String?) {
        self.token = token
        self.email = email
    }
    
}
