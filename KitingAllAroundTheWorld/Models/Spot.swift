//
//  Spot.swift
//  KitingAllAroundTheWorld
//
//  Created by Student on 16/04/2019.
//  Copyright © 2019 none. All rights reserved.
//


import UIKit

class Spot{
    var id : String?
    var name: String?
    var country : String?
    var whenToGo : String?
    var isFavorite: Bool?
    
    init(id: String?, name: String?, country: String?, whenToGo: String?, isFavorite: Bool?){
        self.id = id
        self.name = name
        self.country = country
        self.whenToGo = whenToGo
        self.isFavorite = isFavorite
        
    }
    init(){
        self.id = nil
        self.name = nil
        self.country = nil
        self.whenToGo = nil
        self.isFavorite = nil
        
    }
}
