//
//  SpotCell.swift
//  KitingAllAroundTheWorld
//
//  Created by Student on 19/04/2019.
//  Copyright © 2019 none. All rights reserved.
//

import UIKit

class SpotCell: UITableViewCell {
    
    let cellView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 10
        view.translatesAutoresizingMaskIntoConstraints  = false
        return view
    }()
    let nameLabel: UILabel = {
       let label = UILabel()
        label.text = "Name"
        label.textColor = UIColor.black
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let locationLabel: UILabel = {
        let label = UILabel()
        label.text = "Location"
        label.textColor = UIColor.black
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()


    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupView()
    }
    required init?(coder aDecoder : NSCoder){
        fatalError("init(coder:) has not been implemented")
    }
    func setupView(){
        addSubview(cellView)
        cellView.addSubview(nameLabel)
        cellView.addSubview(locationLabel)
       
        self.selectionStyle = .none
        
        NSLayoutConstraint.activate([
            cellView.topAnchor.constraint(equalTo: self.topAnchor, constant: 20),
            cellView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            cellView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10),
            cellView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10)
            
        ])
        nameLabel.heightAnchor.constraint(equalToConstant: 200).isActive = true
        nameLabel.widthAnchor.constraint(equalToConstant: 200).isActive = true
        nameLabel.centerYAnchor.constraint(equalTo: cellView.centerYAnchor).isActive = true
        nameLabel.leftAnchor.constraint(equalTo: cellView.leftAnchor, constant: 20).isActive = true
      
        
        /*locationLabel.centerYAnchor.constraint(equalTo: cellView.centerYAnchor).isActive = true
        locationLabel.leftAnchor.constraint(equalTo: cellView.leftAnchor, constant: 20).isActive = true*/
    }
    

}
