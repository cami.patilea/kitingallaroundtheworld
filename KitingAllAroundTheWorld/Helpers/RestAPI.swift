//
//  RestAPI.swift
//  KitingAllAroundTheWorld
//
//  Created by Student on 15/04/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Foundation

class RestAPI: NSObject{
    let session = URLSession.shared
    
    let baseURL  = "https://internship-2019.herokuapp.com/"
    
    
    struct UserResult: Codable {
        let result: UserAsStruct
        struct UserAsStruct: Codable {
            let token: String
            let email: String
        }
    }
    
    struct SpotsResult: Codable {
        let result: [SpotsAsStruct]
        struct SpotsAsStruct: Codable {
            let id: String
            let name: String
            let country: String
            let whenToGo: String
            let isFavorite: Bool
        }
    }
    
    struct SpotDetailsResult : Codable{
        let result: SpotDetailsAsStruct
        struct SpotDetailsAsStruct : Codable{
            let id: String
            let name: String
            let longitude : Float
            let latitude : Float
            let windProbability : Int
            let country: String
            let whenToGo: String
            let isFavorite: Bool
        }
    }
    
    struct SpotFavoriteResult : Codable {
        let result: String
    }
    
    struct CountriesResult : Codable {
        let result: [String]
    }
    
    func getUserWithToken(email: String) -> User {
        var result = User(token: nil, email: nil)
        
        let requestURL = URL(string: baseURL + "api-user-get")!
        
        var request = URLRequest(url: requestURL)
        
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let json = [
            "email" : email
        ]
        
        //ignore  exceptions that might appear
        let jsonData = try! JSONSerialization.data(withJSONObject: json, options: [])
        
        let sem = DispatchSemaphore(value: 0)
        
        let task = session.uploadTask(with: request, from: jsonData){ (data, response, error) in
            
            do {
                let user = try JSONDecoder().decode(UserResult.self, from: data!)
                
                result = User(token: user.result.token, email: user.result.email)
                
            } catch {
                print("Error!")
            }
            
            sem.signal()
            
        }
        task.resume()
        sem.wait()
        
        return result
    }
    
    func getAllSpots(token: String, country: String, windProbability: Double)-> [Spot]{
        let requestURL = URL(string : baseURL + "api-spot-get-all")!
        
        var result = [Spot]()
        
        var request = URLRequest(url: requestURL)
        
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(token, forHTTPHeaderField: "token")
        
        let json: [String: Any] = [
            "country" : country,
            "windProbability" : windProbability
            ]
        let jsonData = try! JSONSerialization.data(withJSONObject: json, options: [])
        
         let sem = DispatchSemaphore(value: 0)
        
        let task = session.uploadTask(with: request , from: jsonData){ (data, response, error) in
            do {
                let spots:SpotsResult = try JSONDecoder().decode(SpotsResult.self, from: data!)
               
                for spotResult in spots.result{
                    let spot = Spot(id: spotResult.id, name: spotResult.name, country: spotResult.country, whenToGo: spotResult.whenToGo, isFavorite: spotResult.isFavorite)
                    result.append(spot)
                }
                //print(spots)
            } catch {
                print("error")
            }
            sem.signal()

        }
        task.resume()
        sem.wait()
        return result
    }
    
    func getSpotDetails(token: String, spotID: String)-> SpotDetail{
        let requestURL = URL(string : baseURL + "api-spot-get-details")!
        
        var result = SpotDetail()
        
        var request = URLRequest(url: requestURL)
        
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(token, forHTTPHeaderField: "token")
        
        let json = [
            "spotId" : spotID
        ]
        let jsonData = try! JSONSerialization.data(withJSONObject: json, options: [])
        
        let sem = DispatchSemaphore(value : 0)
        
        let task = session.uploadTask(with: request , from: jsonData){data, response, error in
            
            do{
                let spotDetail = try JSONDecoder().decode(SpotDetailsResult.self, from: data!)
           /* if let data = data, let dataString = String(data: data, encoding: .utf8){
                
                print(dataString)
                }*/
                result = SpotDetail(id: spotDetail.result.id, name: spotDetail.result.name, longitude: spotDetail.result.longitude, latitude: spotDetail.result.latitude, windProbability: spotDetail.result.windProbability, country:  spotDetail.result.country, whenToGo : spotDetail.result.whenToGo, isFavorite : spotDetail.result.isFavorite)
                print(result.country!)
                
            }catch{
                print("error")
            }
            sem.signal()
        }
        task.resume()
        sem.wait()
        return result
    }
    
    func getCountries(token: String) -> [Country]{
        var result = [Country]()
        
        let requestURL = URL(string : baseURL + "api-spot-get-countries")!
        
        var request = URLRequest(url: requestURL)
        
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(token, forHTTPHeaderField: "token")
        
        let jsonData = try! JSONSerialization.data(withJSONObject: [], options: [])
        
        let sem = DispatchSemaphore(value : 0)
        
        let task = session.uploadTask(with: request , from: jsonData){data, response, error in
            do {
                let countries:CountriesResult = try JSONDecoder().decode(CountriesResult.self, from: data!)
                
                for countryResult in countries.result{
                    let country = Country(name: countryResult)
                    result.append(country)
                }
                //print(spots)
            } catch {
                print("error")
            }
            
            sem.signal()
            
        }
        task.resume()
        sem.wait()
        
        return result
    }
    
    func addSpotToFavorites(token: String, spotID: String) -> String{
        let requestURL = URL(string : baseURL + "api-spot-favorites-add")!
        
        var request = URLRequest(url: requestURL)
        
        var result = ""
        
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(token, forHTTPHeaderField: "token")
        
        let json = [
            "spotId" : spotID
        ]
        let jsonData = try! JSONSerialization.data(withJSONObject: json, options: [])
        
        let sem = DispatchSemaphore(value : 0)
        
        let task = session.uploadTask(with: request , from: jsonData){data, response, error in
            
            do {
                let spotFavorite = try JSONDecoder().decode(SpotFavoriteResult.self, from: data!)
                
                result = spotFavorite.result
            }
            catch {
                print("error")
            }

            
            sem.signal()
            
        }
        task.resume()
        sem.wait()
        
        return result
    }
    
    
    func removeSpotFromFavorites(token: String, spotID: String) -> String {
        let requestURL = URL(string : baseURL + "api-spot-favorites-remove")!
        
        var request = URLRequest(url: requestURL)
        
        var result = ""
        
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(token, forHTTPHeaderField: "token")
        
        let json = [
            "spotId" : spotID
        ]
        let jsonData = try! JSONSerialization.data(withJSONObject: json, options: [])
        
        let sem = DispatchSemaphore(value : 0)
        
        let task = session.uploadTask(with: request , from: jsonData){data, response, error in
            do {
                let spotFavorite = try JSONDecoder().decode(SpotFavoriteResult.self, from: data!)
                
                result = spotFavorite.result
            }
            catch {
                print("error")
            }
            
            sem.signal()
            
        }
        task.resume()
        sem.wait()
        
        
        return result
        
    }
    
}

