//
//  LogInController.swift
//  KitingAllAroundTheWorld
//
//  Created by Student on 16/04/2019.
//  Copyright © 2019 none. All rights reserved.
//

import UIKit

class LogInController: UIViewController {
    
    let restAPI = RestAPI()
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var emailInputField: UITextField!
    
    override func viewDidLoad() {
        
        if let cachedUser = UserDefaults.standard.object(forKey: "ApplicationUser") {
            let user:User = try! JSONDecoder().decode(User.self, from: cachedUser as! Data)
            print("avem deja un user")
        }
        else {
            print("nu avem user")
        }
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        
        let email:String = emailInputField.text!
        
        if email == nil || email == "" {
            let alert = UIAlertController(title: "You did not introduce an email", message: "Please fill in with an email adress", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel,  handler: nil))
            self.present(alert, animated: true)
        }
        else {
            let user:User = restAPI.getUserWithToken(email: email)
            if user.token != nil {
                UserDefaults.standard.set(try? JSONEncoder().encode(user), forKey: "ApplicationUser")
                print(user.token!)
                
            }
            else {
                let alert = UIAlertController(title: "This is not a valid email", message: "Please enter a valid email", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel,  handler: nil))
                self.present(alert, animated: true)
                //print(user.token)
            }
            
        }
        
    }
    
 
}
