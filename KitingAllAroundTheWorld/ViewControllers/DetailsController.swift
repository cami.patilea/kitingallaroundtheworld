//
//  DetailsController.swift
//  KitingAllAroundTheWorld
//
//  Created by Student on 16/04/2019.
//  Copyright © 2019 none. All rights reserved.
//

import UIKit

class DetailsController: UIViewController {
    
    var spot = SpotDetail()
    let restAPI = RestAPI()
    
    @IBOutlet weak var longitudeValueLabel: UILabel!
    @IBOutlet weak var latitudeValueLabel: UILabel!
    @IBOutlet weak var countryValueLabel: UILabel!
    @IBOutlet weak var windProbabilityValueLabel: UILabel!
    @IBOutlet weak var favoritesButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupUI() {
        title = spot.name!
        countryValueLabel.text = spot.country!
        windProbabilityValueLabel.text = String(spot.windProbability!)
        longitudeValueLabel.text = String(spot.longitude!)
        latitudeValueLabel.text = String(spot.latitude!)
        if (spot.isFavorite!) {
            favoritesButton.setBackgroundImage(UIImage(named: "star_on.png"), for: .normal, barMetrics: .default)
        }
        else {
            favoritesButton.setBackgroundImage(UIImage(named: "star_off.png"), for: .normal, barMetrics: .default)
        }
    }
 
    @IBAction func favoritesButtonAction(_ sender: UIBarButtonItem) {
        print(spot.isFavorite)
        
        var token = ""
        
        if let cachedUser = UserDefaults.standard.object(forKey: "ApplicationUser") {
            let user:User = try! JSONDecoder().decode(User.self, from: cachedUser as! Data)
            token = user.token!
            
            if spot.isFavorite == false {
                let addToFavoritesResult:String = restAPI.addSpotToFavorites(token: token, spotID: spot.id!)
                
                if (addToFavoritesResult != "") {
                    favoritesButton.setBackgroundImage(UIImage(named: "star_on.png"), for: .normal, barMetrics: .default)
                    spot.isFavorite = true
                }
                
                print(spot.isFavorite)
            }
            else {
                let removeFromFavoritesResult:String = restAPI.removeSpotFromFavorites(token: token, spotID: spot.id!)
                
                if (removeFromFavoritesResult != "") {
                    favoritesButton.setBackgroundImage(UIImage(named: "star_off.png"), for: .normal, barMetrics: .default)
                    spot.isFavorite = false
                }
                
                print(spot.isFavorite)
            }
        }
        else {
            print("nu avem user")
        }
    }
}
