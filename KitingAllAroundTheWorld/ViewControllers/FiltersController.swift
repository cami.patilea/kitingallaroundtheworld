//
//  FiltersController.swift
//  KitingAllAroundTheWorld
//
//  Created by Student on 16/04/2019.
//  Copyright © 2019 none. All rights reserved.
//

import UIKit
import DropDown

protocol FiltersControllerDelegate: class {
    func filterController(_ filterController: FiltersController, didSelect item: String)
    func pressSaveButton(_ filterController: FiltersController, didSelect item: String, didInput item_1: String)
    func didSave(_ filterController: FiltersController, didSelect item: String, didInput item2: Double)
}

class FiltersController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {

    var countries = [Country]()
    
    var selectedCountry = ""
    var selectedWindProbability = ""
    
    @IBOutlet weak var saveFiltersButton: UIBarButtonItem!
    @IBOutlet private weak var textField: UITextField!
    @IBOutlet private weak var pickerView: UIPickerView?
    @IBOutlet weak var windProbabilityTextField: UITextField!
    
    weak var delegate: FiltersControllerDelegate?
    
    private var customInputView: UIView?
    override var inputAccessoryView: UIView? {
        return customInputView
    }
    
    @IBAction func saveFiltersButtonAction(_ sender: UIBarButtonItem) {
        selectedWindProbability = windProbabilityTextField.text!
        
        delegate?.pressSaveButton(self, didSelect: selectedCountry, didInput: selectedWindProbability)
        delegate?.didSave(self, didSelect: selectedCountry, didInput: Double(selectedWindProbability) ?? 0.0)
        
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction private func selectCountryDidTap(_ sender: UIButton?) {
        customInputView = pickerView
        becomeFirstResponder()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        customInputView = nil
        
        view.endEditing(true)
        resignFirstResponder()
    }
    
    override var canBecomeFirstResponder: Bool {
        return customInputView != nil
    }

    
    //MARK: - UIPickerViewDataSource
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countries.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return countries[row].name!
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedCountry = countries[row].name!
        delegate?.filterController(self, didSelect: countries[row].name!)
    }
    
    //MARK: - TextFieldDelegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        customInputView = nil
    }
    
   
    
}
