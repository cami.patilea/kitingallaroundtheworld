//
//  SpotsController.swift
//  KitingAllAroundTheWorld
//
//  Created by Student on 15/04/2019.
//  Copyright © 2019 none. All rights reserved.
//

import UIKit

class SpotsController: UIViewController, UITableViewDelegate, UITableViewDataSource, FiltersControllerDelegate {
    func pressSaveButton(_ filterController: FiltersController, didSelect item: String, didInput item_1: String) {
        //
    }
    

    let tableview : UITableView = {
        let tv = UITableView()
        tv.backgroundColor = UIColor.white
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.separatorColor = UIColor.white
        return tv
        
    }()
    
    @IBOutlet weak var spotsTable: UITableView!
    
    var spots = [Spot]()
    let restAPI = RestAPI()
    
    var filterWindProbability = 0.0
    var filterCountry = ""
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setupTableView()

        var token = ""
        
        if let cachedUser = UserDefaults.standard.object(forKey: "ApplicationUser") {
            let user:User = try! JSONDecoder().decode(User.self, from: cachedUser as! Data)
            //print("avem deja un user")
            token = user.token!
        }
        else {
            print("nu avem user")
        }
        
        spots = restAPI.getAllSpots(token: token, country: filterCountry, windProbability: filterWindProbability)
        
        /*for spot in spots
        {
            print(spot.country!)
            print(spot.id!)
            print(spot.isFavorite!)
            print(spot.name!)
            print(spot.whenToGo!)
        }*/

        // Do any additional setup after loading the view.
    }
    
   
    
    func setupTableView(){
        tableview.delegate = self
        tableview.dataSource = self
        tableview.register(SpotCell.self, forCellReuseIdentifier: "cellId")
        view.addSubview(tableview)
        
        NSLayoutConstraint.activate([
            tableview.topAnchor.constraint(equalTo: self.view.topAnchor),
            tableview.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            tableview.rightAnchor.constraint(equalTo: self.view.rightAnchor),
            tableview.leftAnchor.constraint(equalTo: self.view.leftAnchor)
        ])
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return spots.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "cellId", for: indexPath) as! SpotCell
        cell.backgroundColor = UIColor.white
        cell.nameLabel.text = spots[indexPath.row].name!
        cell.locationLabel.text = spots[indexPath.row].country! 
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    var selectedSpot : Spot?
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        let spot = spots[indexPath.row]
    
    selectedSpot = spot
    
    performSegue(withIdentifier: "Details", sender: nil)
    
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let segueIdentifier = segue.identifier else { return }
        
        switch segueIdentifier {
        case "Details":
            var token = ""
            let spotDetail = segue.destination as! DetailsController
            
            if let cachedUser = UserDefaults.standard.object(forKey: "ApplicationUser") {
                let user:User = try! JSONDecoder().decode(User.self, from: cachedUser as! Data)
                token = user.token!
            }
            else {
                print("nu avem user")
            }
            if token != "" {
                let details:SpotDetail = restAPI.getSpotDetails(token: token, spotID: selectedSpot?.id! ?? "")
                spotDetail.spot = details
                
            }
        case "Filters":
            var token = ""
            let destinationVc = segue.destination as? FiltersController
            
            if let cachedUser = UserDefaults.standard.object(forKey: "ApplicationUser") {
                let user:User = try! JSONDecoder().decode(User.self, from: cachedUser as! Data)
                token = user.token!
            }
            else {
                print("nu avem user")
            }
            
            if token != "" {
                let countries:[Country] = restAPI.getCountries(token: token)
                destinationVc?.countries = countries
                
            }
            
            // destinationVc?.countries = ["ALBANIA", "ROMANIA", "UK", "SMILEY", "!RUBY"]
            destinationVc?.delegate = self
        default: return
        }
    }
    
    func filterController(_ filterController: FiltersController, didSelect item: String) {
        print("FILTER BY \(item)")
    }
    
//    func pressSaveButton(_ filterController: FiltersController, didSelect item: String, didInput item_1: String) {
//        print(item)
//        print(item_1)
//
//        filterCountry = item
//        filterWindProbability = item_1
//    }
    
    
    @IBAction func unwindToSpotsController(_ segue: UIStoryboardSegue) {
        
    }
    
    func didSave(_ filterController: FiltersController, didSelect item: String, didInput item2: Double) {
        print(item)
        filterCountry = item
        filterWindProbability = item2
        
        var token = ""
        
        if let cachedUser = UserDefaults.standard.object(forKey: "ApplicationUser") {
            let user:User = try! JSONDecoder().decode(User.self, from: cachedUser as! Data)
            //print("avem deja un user")
            token = user.token!
        }
        else {
            print("nu avem user")
        }
        spots = restAPI.getAllSpots(token: token, country: filterCountry, windProbability: filterWindProbability)
        tableview.reloadData()
    }

}
